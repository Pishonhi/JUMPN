# SWIMN

### SWIMN is a web 3.0 lifestyle app that implements the jump-to-earn concept.

![Build Status](https://765022154-files.gitbook.io/~/files/v0/b/gitbook-x-prod.appspot.com/o/spaces%2FzCTUiyBFEsve5x72fsjA%2Fuploads%2FguWD4Sobj9ZlXasJcNwL%2Fbanner_gitbook.png?alt=media&token=d160cfd0-b21f-424f-b592-6d26c0f19cd2)

SWIMN is a web 3.0 lifestyle app that implements the swim-to-earn concept. SWIMN is designed to bring happiness back to people through three dimensions: social connections, physical health, and, most importantly, income. Users of SWIMN will gain a high return of $SWN through 'Swimming.' There are several modes, including backstroke mode, breaststroke mode, and extreme sports mode, which contain different rewards levels. 

## MODE

- Freestyle Mode
- Backstroke Mode
- Breaststroke Mode
- Extreme Sports Mode

## Swim To Earn Mechanics

### Key Factors

##### Factors that determine the output of your SWN:

-  Efficiency level of your snorkels, swimfins, or goggles.
-  Frequencies of your movement.
-  Swimming ability.
-  Time you spent.

### Daily Limit

- Backstroke Mode<br>
Time limitation: 4 hours per day
- Breaststroke Mode<br>
Time limitation: 0.5 hours per day
- Freestyle Mode<br>
Calories calculated in 24 hours per day<br>
- Extreme Mode(Coming Soon)

### Earning Mechanics

#### For Backstroke and Breaststroke Mode:

##### _SWN Rewards = N * WAEL*(WABA+ABA)^SWIMN value * α(P1,P2)*F*100_

- N = Number of SET burned.
- WAEL = Weighted average of efficiency level Index.
- WABA = Weighted average of efficiency level.
- ABA = Additional swimming ability.
- F = Ratio of average frequency per minute
- α = Alpha coefficient 
- Parameter: A range of system values.
- SWIMN value: Value set by the team. It changes following the percentage of users engagement. 


#### For Freestyle Mode

- You will get a fixed reward of 10,000 SWN if over 2000 calories are burned per day. 
- If less than 2000 calories burned, SWN Rewards =(Actual Calories you burned - 800)/12*100
  For example, if only 1300 calories are burned today, you will earn (1300-800)/12*100 = 4166.7 SWN as your normal mode reward. 

#### For Extreme Sports Mode (Coming Soon)

Extreme Sports Mode can be used twice per week. You will need to record a video or start a live stream for your challenge. The likes and comments you got and the challenges' difficulties will be directly reflected in the rewards. 

# TOKEN

### SET

SET has an unlimited supply and is earned when a user staked or opens the mystery combo box.<br>
The JET is burned by:<br>
Wristband-Minting<br>
Goggles-Minting<br>
Swimfin-Minting<br>
Outfit Repair<br>
Joining activities in different modes<br>
Leveling up swimfins, goggles, and snorkel<br>
Ability upgrade

### Tokenomics

The initial supply of SET is 10,000,000. There are 100,000 SET minted per 24 hours for staking pools. SWIMN members have the right to vote for daily output of SET one week after SWIMN farm released.

## SWIMN

### Tokenomics

![Build Status](https://765022154-files.gitbook.io/~/files/v0/b/gitbook-x-prod.appspot.com/o/spaces%2FzCTUiyBFEsve5x72fsjA%2Fuploads%2FOLaI6CDRcYSKri2oGfpH%2FJSTTOKENMETRICS2.png?alt=media&token=100b5c1f-1e6f-47e4-b690-57c6b934857b)

## Airdrop

1,294,600,000 SWN (1.2946%) are reserved for airdrop. Early members who finished our airdrop tasks and SWIMN ambassadors will get the airdrop when launched. Please submit the relevant materials to hello@swimn.today to get your airdrop allocation. 

## Anti-Cheating System

Four mechanics are used to stop cheating in the game, which include the cheater's account being punishment:

1. GPS tracking
2. Data edit by coding
3. machine simulation learning through data
4. 24 hours data swimming tracking

Each account must only match one snorkel (SWIMN). If an investor transfer snorkel to another accounting system properly will lock up or burn in the marketplace.

## Team

![Build Status](https://765022154-files.gitbook.io/~/files/v0/b/gitbook-x-prod.appspot.com/o/spaces%2FzCTUiyBFEsve5x72fsjA%2Fuploads%2F4aqUf3H0hM5IuflAYSxt%2FTHETEAM.png?alt=media&token=9a148009-7681-43e7-9a36-3075788d016c)

## Roadmap
![Build Status](https://765022154-files.gitbook.io/~/files/v0/b/gitbook-x-prod.appspot.com/o/spaces%2FzCTUiyBFEsve5x72fsjA%2Fuploads%2FiNmRaDKHVxDqQ9A3mXvY%2Froadmap.png?alt=media&token=7374bd96-25f7-4a27-9c91-19c888c0568a)

## Social and Community

SWIMN aims to build a solid social community with people around the world. Social-Fi DAO will be grouped by players. The community will poll for future decisions. Such as organizing swimming competitions. More details will be revealed in the future. 

## Useful Links

Website: https://swimn.today<br>
Telegram: https://t.me/SWIMNOFFICIAL<br>

